///<reference path="sortingAlgorithms/bubbleSort.ts"/>
///<reference path="sortingAlgorithms/insertionSort.ts"/>
///<reference path="sortingAlgorithms/quickSort.ts"/>
///<reference path="shuffleAlgorithms/yatesFisherShuffle.ts"/>
///<reference path="recursionAlgorithms/findFactorial.ts"/>
///<reference path="recursionAlgorithms/findFibonacci.ts"/>
///<reference path="recursionAlgorithms/findTribonacci.ts"/>
module Algorithms {
     class Algorithms {
        array: Array<number>;

        constructor() {
            this.array = new Array<number>();
            this.array.push(10);
            this.array.push(1);
            this.array.push(8);
            this.array.push(5);
            this.array.push(4);

            let result;

            result = document.getElementById('item1');
            result.innerHTML += this.array;

            result = document.getElementById('item2');
            console.time('yatesFisherShuffle');
            result.innerHTML += this.array.yatesFisherShuffle();
            console.timeEnd('yatesFisherShuffle');

            result = document.getElementById('item3');
            console.time('bubbleSort');
            result.innerHTML += this.array.bubbleSort();
            console.timeEnd('bubbleSort');

            this.array.yatesFisherShuffle();

            result = document.getElementById('item4');
            console.time('insertionSort');
            result.innerHTML += this.array.insertionSort();
            console.timeEnd('insertionSort');

            result = document.getElementById('item5');
            console.time('findFactorial');
            result.innerHTML += findFactorial(6);
            console.timeEnd('findFactorial');

            result = document.getElementById('item6');
            console.time('findFibonacci');
            result.innerHTML += findFibonacci(30);
            console.timeEnd('findFibonacci');

            result = document.getElementById('item7');
            console.time('findTribonacci');
            result.innerHTML += findTribonacci(9);
            console.timeEnd('findTribonacci');

            this.array.yatesFisherShuffle();
            result = document.getElementById('item8');
            console.time('quickSort');
            this.array = quickSort(this.array);
            console.timeEnd('quickSort');
            result.innerHTML += this.array;


        }
    }
    window.onload = () => {
        new Algorithms();
    };
}