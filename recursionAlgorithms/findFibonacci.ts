//0, 1, 1, 2, 3, 5, 8, 13, 21, 34....
function findFibonacci(n) {
    if (n <= 1) {
        return 1;
    }

    return findFibonacci(n - 1) + findFibonacci(n - 2);
}