//0, 1, 1, 2, 4, 7, 13, 24, 44, 81...
function findTribonacci(n) {
    if (n <= 2) {
        return 0;
    }
    if (n == 3) {
        return 1;
    }
    else {
        return findTribonacci(n - 1) + findTribonacci(n - 2) + findTribonacci(n - 3);
    }
}