Array.prototype.bubbleSort = function () {
    for (let i = 0; i < this.length; i++) {
        for (let j = 0; j < this.length - 1; j++) {
            if (this[j] > this[i]) {
                let temp = this[i];
                this[i] = this[j];
                this[j] = temp;
            }
        }
    }
    return this;
};
//# sourceMappingURL=bubbleSort.js.map