
function quickSort(array){
    if(array.length < 2 ) {
        return array;
    }

    let pivot = array[0];
    let left = [];
    let right = [];

    for(let i = 1; i < array.length; i++) {
        if(array[i] < pivot) {
            left.push(array[i]);
        } else {
            right.push(array[i]);
        }
    }

    return quickSort(left).concat(pivot, quickSort(right));
}

/*
function quickSort(array, left, right) {
    var pivot = partition(array, left, right);

    if(left < pivot - 1) {
        quickSort(array, left, pivot - 1);
    }
    if(right > pivot) {
        quickSort(array, pivot, right);
    }
    return array;
}

function partition(array, left, right){
    var pivot = Math.floor((left + right) / 2 );

    while(left <= right) {
        while(array[left] < array[pivot]) {
            left++;
        }
        while(array[right] > array[pivot]) {
            right--;
        }
        if(left <= right) {
            let temp = array[left];
            array[left] = array[right];
            array[right] = temp;
            left++;
            right--;
        }
    }
    return left;
}
*/
