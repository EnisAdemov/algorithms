interface Array<T> {
    yatesFisherShuffle(): Array<T>;
    randomNumber: number;
}

Array.prototype.yatesFisherShuffle = function () {
    for(let i = 0; i < this.length; i++) {
        this.randomNumber = Math.floor(Math.random() * this.length);
        let temp = this[i];
        this[i] = this[this.randomNumber];
        this[this.randomNumber] = temp;
    }
    return this;
};
